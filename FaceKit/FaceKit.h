/*
Copyright 2020-2021 Panic Inc.

This file is part of Audion.

Audion is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Audion is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Audion.  If not, see <https://www.gnu.org/licenses/>.
*/

#import <Foundation/Foundation.h>

//! Project version number for FaceKit.
FOUNDATION_EXPORT double FaceKitVersionNumber;

//! Project version string for FaceKit.
FOUNDATION_EXPORT const unsigned char FaceKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FaceKit/PublicHeader.h>


